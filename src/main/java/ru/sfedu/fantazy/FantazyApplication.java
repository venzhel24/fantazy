package ru.sfedu.fantazy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FantazyApplication {

	public static void main(String[] args) {
		SpringApplication.run(FantazyApplication.class, args);
	}

}
