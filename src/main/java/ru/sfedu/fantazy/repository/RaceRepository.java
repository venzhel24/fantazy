package ru.sfedu.fantazy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import ru.sfedu.fantazy.model.Event;
import ru.sfedu.fantazy.model.Race;
import ru.sfedu.fantazy.model.RaceType;
import ru.sfedu.fantazy.model.Result;

import java.util.List;

public interface RaceRepository extends JpaRepository<Race, Long> {
    List<Race> findByEventId(long EventId);

    boolean existsByEventAndRaceType(Event event, RaceType raceType);
}
