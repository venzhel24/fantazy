package ru.sfedu.fantazy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.sfedu.fantazy.model.Individual;
import ru.sfedu.fantazy.model.Sprint;

import java.util.List;

public interface IndividualRepository extends JpaRepository<Individual, Long> {
    List<Individual> findByRaceId(long RaceId);
}
