package ru.sfedu.fantazy.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.sfedu.fantazy.model.Pursuit;
import ru.sfedu.fantazy.model.Sprint;

import java.util.List;

public interface PursuitRepository extends JpaRepository<Pursuit, Long> {
    List<Pursuit> findByRaceId(long RaceId);
}
