package ru.sfedu.fantazy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.sfedu.fantazy.model.Team;

import java.util.List;
import java.util.Optional;

@Repository
public interface TeamRepository extends JpaRepository<Team, Long> {
    Optional<Team> findByEventIdAndUserId(long eventId, long UserId);
    List<Team> findByEventId(long eventId);
    boolean existsTeamByEventIdAndUserId(long eventId, long userId);
}
