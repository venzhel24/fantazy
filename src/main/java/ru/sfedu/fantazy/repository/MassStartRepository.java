package ru.sfedu.fantazy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.sfedu.fantazy.model.MassStart;
import ru.sfedu.fantazy.model.Sprint;

import java.util.List;

public interface MassStartRepository extends JpaRepository<MassStart, Long> {
    List<MassStart> findByRaceId(long RaceId);
}
