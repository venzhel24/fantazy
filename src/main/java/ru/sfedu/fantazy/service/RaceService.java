package ru.sfedu.fantazy.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.sfedu.fantazy.model.Event;
import ru.sfedu.fantazy.model.Race;
import ru.sfedu.fantazy.model.RaceType;
import ru.sfedu.fantazy.repository.RaceRepository;
import ru.sfedu.fantazy.repository.ResultRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RaceService {
    private final RaceRepository raceRepository;

    public Race getById(long id) {
        return raceRepository.findById(id).orElse(null);
    }

    public List<Race> getAll() {
        List<Race> races = raceRepository.findAll();
        if (races.isEmpty()) {
            return null;
        }
        return races;
    }

    public List<Race> getAllByEventId(long eventId) {
        List<Race> races = raceRepository.findByEventId(eventId);
        if (races.isEmpty()) {
            return null;
        }
        return races;
    }

    public Race saveRace(Race race) {
        return raceRepository.save(race);
    }

    public void deleteById(long id) {
        raceRepository.deleteById(id);
    }

    public boolean isRaceExistsByEventAndRaceType(Event event, RaceType raceType) {
        return raceRepository.existsByEventAndRaceType(event, raceType);
    }
}