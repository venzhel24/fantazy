package ru.sfedu.fantazy.service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.sfedu.fantazy.dto.AthleteAndPoints;
import ru.sfedu.fantazy.dto.OverviewTeamDto;
import ru.sfedu.fantazy.dto.TeamAndPointsDto;
import ru.sfedu.fantazy.dto.TeamDto;
import ru.sfedu.fantazy.model.Athlete;
import ru.sfedu.fantazy.model.Team;
import ru.sfedu.fantazy.model.Event;
import ru.sfedu.fantazy.model.User;
import ru.sfedu.fantazy.repository.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class TeamService {
    private final ResultRepository resultRepository;
    private final TeamRepository teamRepository;
    private final AthleteRepository athleteRepository;
    private final EventRepository eventRepository;

    private final UserService userService;

    public Team getTeamById(long teamId) {
        return teamRepository.findById(teamId).orElse(null);
    }

    public List<Team> getTeamsByEvent(long eventId) {
        List<Team> teams = teamRepository.findByEventId(eventId);
        if (teams.isEmpty()) {
            return null;
        }
        return teams;
    }

    public List<TeamAndPointsDto> getTeamsAndPointsListByEventId(long eventId) {
        try {
            log.debug("getTeamAndPointsListByEventId start:");
            List<Team> teams = teamRepository.findByEventId(eventId);
            List<TeamAndPointsDto> teamAndPointsDtoList = new ArrayList<>();

            if (teams.isEmpty()) {
                return null;
            }

            teamAndPointsDtoList = teams.stream()
                    .map(team -> TeamAndPointsDto.builder()
                            .team(team)
                            .totalPoints(getPointsByTeam(team))
                            .build())
                    .sorted(Comparator.comparingInt(TeamAndPointsDto::getTotalPoints).reversed())
                    .collect(Collectors.toList());

            return teamAndPointsDtoList;
        } catch (Exception e) {
            log.error("getTeamAndPointsListByEventId Error");
            log.error(e.getClass().getName() + ": " + e.getMessage());
            return null;
        }
    }


    public OverviewTeamDto getTeamAndPointsByTeam(Team team) {
        try {
            log.debug("getOverviewTeamByTeam start");
            List<Athlete> athletes = new ArrayList<>(Arrays.asList(team.getAthlete1(), team.getAthlete2(), team.getAthlete3()));
            List<AthleteAndPoints> athleteAndPointsList = new ArrayList<>();
            for (Athlete athlete : athletes) {
                AthleteAndPoints athleteAndPoints = AthleteAndPoints.builder()
                        .athlete(athlete)
                        .points(resultRepository.findPointsByAthleteIdAndEventId(athlete.getId(), team.getEvent().getId()).orElse(0))
                        .build();
                athleteAndPointsList.add(athleteAndPoints);
            }
            OverviewTeamDto overviewTeamDto = OverviewTeamDto.builder()
                    .team(team)
                    .athleteAndPointsList(athleteAndPointsList)
                    .build();
            overviewTeamDto.calculateTotalPoints();
            return overviewTeamDto;
        } catch (Exception e) {
            log.error("getOverviewTeamByTeam Error");
            log.error(e.getClass().getName() + ": " + e.getMessage());
            return null;
        }
    }

    /**
     * The getTeamByEventIdAndUsername function is used to retrieve a team by the event and username.
     *
     * @param long eventId Get the event object from the database
     *
     * @return A team object
     */
    public Team getTeamByEventAndUser(Event event) {
        try {
            log.debug("getTeamByEventIdAndUserId start[1]:");
            User currentUser = userService.getCurrentUser();
            long currentUserId = currentUser.getId();
            Team team = teamRepository.findByEventIdAndUserId(event.getId(), currentUserId)
                    .orElseThrow(() -> new IllegalArgumentException("Team not found with event ID " + event.getId() + " and user ID " + currentUserId));
            log.debug("Team retrieved: {}", team);
            return team;
        } catch (Exception e) {
            log.error("getTeamByEventIdAndUserId Error");
            log.error(e.getClass().getName() + ": " + e.getMessage());
            return null;
        }
    }

    /**
     * The addNewTeam function is used to add a new team to the database.
     * It takes in an event ID, a list of sportsman IDs, and a team name as parameters.
     * The function first checks if the user is already registered for the tournament by checking if there exists any teams with that user's ID and that event's ID in the database. If so, it throws an exception saying &quot;User is already registered for this tournament;.
     * Next it checks if exactly 3 sportsmen are required by checking if there are exactly 3 elements in the list of sportsman IDs passed into this function as parameter.
     *
     * @param long eventId Retrieve the event from the database
     * @param List<Long> sportsmanIds Pass the ids of the athletes that will be in the team
     * @param String team_name Set the name of the team
     *
     * @return Team that has been saved
     */
    @Transactional
    public Team addNewTeam(TeamDto teamDto) {
        try {
            log.debug("addNewTeam start[1]:");
            Event event = eventRepository.findById(teamDto.getEventId())
                    .orElseThrow(() -> new IllegalArgumentException("Invalid event id"));
            User currentUser = userService.getCurrentUser();

            if (teamRepository.existsTeamByEventIdAndUserId(teamDto.getEventId(), currentUser.getId())) {
                throw new IllegalArgumentException("User is already registered for this tournament");
            }

            List<Athlete> athleteList = validateAthleteIds(teamDto.getAthleteIds());

            Team team = Team.builder()
                    .name(teamDto.getName())
                    .event(event)
                    .user(currentUser)
                    .athlete1(athleteList.get(0))
                    .athlete2(athleteList.get(1))
                    .athlete3(athleteList.get(2))
                    .build();
            log.debug("addNewTeam team added [2]: {}", team);
            return teamRepository.save(team);
        } catch (Exception e) {
            log.error("addNewTeam Error");
            log.error(e.getClass().getName() + ": " + e.getMessage());
            return null;
        }
    }

    public int getPointsByTeam(Team team) {
        int totalPoints = 0;
        totalPoints += resultRepository.findPointsByAthleteIdAndEventId(
                team.getAthlete1().getId(), team.getEvent().getId()).orElse(0);
        totalPoints += resultRepository.findPointsByAthleteIdAndEventId(
                team.getAthlete2().getId(), team.getEvent().getId()).orElse(0);
        totalPoints += resultRepository.findPointsByAthleteIdAndEventId(
                team.getAthlete3().getId(), team.getEvent().getId()).orElse(0);
        return totalPoints;
    }

    private List<Athlete> validateAthleteIds(List<Long> athleteIds) {
        if (athleteIds.size() != 3) {
            throw new IllegalArgumentException("Exactly 3 sportsmen are required");
        }
        List<Athlete> athletes = new ArrayList<>();
        athletes.add(athleteRepository.findById(athleteIds.get(0))
                .orElseThrow(() -> new IllegalArgumentException("Invalid athlete id")));
        athletes.add(athleteRepository.findById(athleteIds.get(1))
                .orElseThrow(() -> new IllegalArgumentException("Invalid athlete id")));
        athletes.add(athleteRepository.findById(athleteIds.get(2))
                .orElseThrow(() -> new IllegalArgumentException("Invalid athlete id")));
        return athletes;
    }
}
