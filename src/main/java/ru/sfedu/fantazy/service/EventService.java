package ru.sfedu.fantazy.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.sfedu.fantazy.model.Event;
import ru.sfedu.fantazy.repository.EventRepository;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class EventService {
    private final EventRepository eventRepository;

    public List<Event> getAll() {
        List<Event> events = eventRepository.findAll();
        if (events.isEmpty()) {
            return null;
        }
        return events;
    }

    public Event saveEvent(Event event) {
        return eventRepository.save(event);
    }

    public void deleteById(long id) {
        eventRepository.deleteById(id);
    }

    public Event getById(long id) {
        return eventRepository.findById(id).orElse(null);
    }

    /**
     * The getCurrentTournament function returns the current tournament.
     *
     * @return The event that is currently running
     */
    public Event getCurrentTournament() {
        try {
            log.debug("getCurrentTournament start[1]:");
            List<Event> events = eventRepository.findAll();
            Event event = events.stream()
                    .filter(t -> {
                        LocalDate startDate = t.getStartDate();
                        LocalDate endDate = t.getEndDate();
                        return !LocalDate.now().isBefore(startDate) && !LocalDate.now().isAfter(endDate);
                    })
                    .findFirst()
                    .orElse(null);
            log.debug("Current tournament [2]: {}", event);
            return event;
        } catch (Exception e) {
            log.error("getCurrentTournament Error");
            log.error(e.getClass().getName() + ":" + e.getMessage());
            return null;
        }
    }

    public Event getCurrentEvent() {
        try {
            log.debug("getCurrentEvent start[1]:");
            Event event = eventRepository.findCurrentEvent(LocalDate.now())
                    .orElseThrow(() -> new Exception("No current event"));
            log.debug("Current event [2]: {}", event);
            return event;
        } catch (Exception e) {
            log.error("getCurrentEvent Error");
            log.error(e.getClass().getName() + ":" + e.getMessage());
            return null;
        }
    }

    /**
     * The getFutureTournament function returns the next tournament that is scheduled to occur.
     *
     * @return The next tournament
     */
    public Event getFutureTournament() {
        try {
            log.debug("getFutureTournament start[1]:");
            List<Event> events = eventRepository.findAll();
            Event event = events.stream()
                    .filter(x -> x.getStartDate().isAfter(LocalDate.now()))
                    .min(Comparator.comparing(x -> {
                        LocalDate start_date = x.getStartDate();
                        return Math.abs(ChronoUnit.DAYS.between(LocalDate.now(), start_date));
                    }))
                    .orElse(null);
            log.debug("Future tournament retrieved[2]: {}", event);
            return event;
        } catch (Exception e) {
            log.error("getFutureTournament Error");
            log.error(e.getClass().getName() + ":" + e.getMessage());
            return null;
        }
    }

    public Event getNextEvent() {
        try {
            log.debug("getNextEvent start[1]:");
            LocalDate today = LocalDate.now();
            List<Event> events = eventRepository.findNextEvents(today);
            if (events.isEmpty()) {
                return null;
            }
            Event event = events.get(0);
            log.debug("Next tournament [2]: {}", event);
            return event;
        } catch (Exception e) {
            log.error("getNextEvent Error");
            log.error(e.getClass().getName() + ":" + e.getMessage());
            return null;
        }
    }

    /**
     * The getPastTournament function returns the most recent past tournament.
     *
     * @return The event that ended the closest to today
     */
    public Event getPastTournament() {
        try {
            log.debug("getPastTournament start[1]:");
            List<Event> events = eventRepository.findAll();
            Event event = events.stream()
                    .filter(x -> x.getEndDate().isBefore(LocalDate.now()))
                    .min(Comparator.comparing(x -> {
                        LocalDate end_date = x.getEndDate();
                        return Math.abs(ChronoUnit.DAYS.between(LocalDate.now(), end_date));
                    }))
                    .orElse(null);
            log.debug("Past tournament [2]: {}", event);
            return event;
        } catch (Exception e) {
            log.error("getPastTournament Error");
            log.error(e.getClass().getName() + ":" + e.getMessage());
            return null;
        }
    }

    public Event getLastEvent() {
        try {
            log.debug("getLastEvent start[1]:");
            LocalDate today = LocalDate.now();
            List<Event> events = eventRepository.findLastEvents(today);
            if (events.isEmpty()) {
                return null;
            }
            Event event = events.get(0);
            log.debug("Last tournament [2]: {}", event);
            return event;
        } catch (Exception e) {
            log.error("getLastEvent Error");
            log.error(e.getClass().getName() + ":" + e.getMessage());
            return null;
        }
    }
}
