package ru.sfedu.fantazy.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.sfedu.fantazy.model.Athlete;
import ru.sfedu.fantazy.repository.AthleteRepository;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class AthleteService {
    private final AthleteRepository athleteRepository;

    public List<Athlete> getAll() {
        List<Athlete> athletes = athleteRepository.findAll();
        if (athletes.isEmpty()) {
            return null;
        }
        return athletes;
    }

    public boolean addPhoto(long athleteId, MultipartFile photo) {
            try {
                log.debug("Start addPhoto");
                if (photo.isEmpty()) {
                    log.debug("addPhoto: photo is empty");
                    return false;
                }
                Athlete athlete = athleteRepository.findById(athleteId)
                        .orElseThrow(() -> new IllegalArgumentException("Invalid athlete id"));
                String base64EncodedImage = Base64.encodeBase64String(photo.getBytes());
                athlete.setPhoto(base64EncodedImage);
                athleteRepository.save(athlete);
                log.debug("addPhoto successfully added");
                return true;
            } catch (Exception e) {
                log.error("addPhoto Error");
                log.error(e.getClass().getName() + ":" + e.getMessage());
                return false;
            }
    }


}
