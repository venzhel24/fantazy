package ru.sfedu.fantazy.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.sfedu.fantazy.dto.AthleteAndPoints;
import ru.sfedu.fantazy.model.Athlete;
import ru.sfedu.fantazy.model.Race;
import ru.sfedu.fantazy.model.RaceType;
import ru.sfedu.fantazy.model.Result;
import ru.sfedu.fantazy.repository.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class ResultsService {
    private final ResultRepository resultRepository;
    private final IndividualRepository individualRepository;
    private final SprintRepository sprintRepository;
    private final MassStartRepository massStartRepository;
    private final PursuitRepository pursuitRepository;
    private final RaceRepository raceRepository;
    private final AthleteRepository athleteRepository;

    /**
     * The getAllResultsByRace function is used to retrieve all results for a given race.
     *
     * @param long race_id Find the race in the database
     *
     * @return A list of the correct type, but only if i use the following code:
     */
    public <T extends Result> List<T> getAllResultsByRace(long race_id) {
        try {
            log.debug("getAllResultsByRace start [1]:");
            Race race = raceRepository.findById(race_id)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid race id"));
            RaceType race_type = race.getRaceType();
            List<T> results;

            if (race_type.equals(RaceType.MASS_START)) {
                results = (List<T>) massStartRepository.findByRaceId(race_id);
                log.debug("MassStart results retrieved [2]:");
                return results;
            }

            if (race_type.equals(RaceType.PURSUIT)) {
                results = (List<T>) pursuitRepository.findByRaceId(race_id);
                log.debug("Pursuit results retrieved [2]:");
                return results;
            }

            if (race_type.equals(RaceType.INDIVIDUAL)) {
                results = (List<T>) individualRepository.findByRaceId(race_id);
                log.debug("Individual results retrieved [2]:");
                return results;
            }

            if (race_type.equals(RaceType.SPRINT)) {
                results = (List<T>) sprintRepository.findByRaceId(race_id);
                log.debug("Sprint results retrieved [2]:");
                return results;
            }

            throw new IllegalArgumentException("Invalid race type");
        } catch (Exception e) {
            log.error("getAllResultsByRace Error");
            log.error(e.getClass().getName() + ": " + e.getMessage());
            return null;
        }
    }

    /**
     * The getPointsByEventAndAthleteIds function takes in an eventId and a list of athleteIds.
     * It returns a list of AthleteAndPoints objects, which contain the specified athletes' names and points earned in the specified event.
     * If an athlete has not participated in the specified event, their points will be - 1.
     *
     * @param long eventId Specify the event for which the points are being retrieved
     * @param List&lt;Long&gt; athleteIds Pass a list of athlete ids to the function
     *
     * @return A list of athleteandpoints objects
     */
    public List<AthleteAndPoints> getPointsByEventAndAthleteIds(long eventId, List<Long> athleteIds){
        try{
            log.debug("getPointsByEventAndAthlete start [1]:");
            List<AthleteAndPoints> athleteAndPointsList = new ArrayList<>();

            for(long athleteId : athleteIds) {
                Athlete athlete = athleteRepository.findById(athleteId)
                        .orElseThrow(() -> new IllegalArgumentException("Invalid athlete id"));

                // Get the points earned by the specified athlete in the specified event.
                Optional<Integer> athleteEventPoints = resultRepository.findPointsByAthleteIdAndEventId(athleteId, eventId);
                if (athleteEventPoints.isEmpty()) {
                    athleteEventPoints = Optional.of(0);
                }
                AthleteAndPoints athleteAndPoints = AthleteAndPoints.builder()
                        .athlete(athlete)
                        .points(athleteEventPoints.get())
                        .build();
                athleteAndPointsList.add(athleteAndPoints);
            }
            log.debug("points retrieved [2]:");
            return athleteAndPointsList;
        } catch (Exception e) {
            log.error("getPointsByEventAndAthlete Error");
            log.error(e.getClass().getName() + ": " + e.getMessage());
            return null;
        }
    }

    public void deleteByRace(Race race) {
        resultRepository.deleteAllByRace(race);
    }

}
