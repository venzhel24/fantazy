package ru.sfedu.fantazy.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.sfedu.fantazy.model.*;
import ru.sfedu.fantazy.service.*;

@Controller
@RequiredArgsConstructor
public class MainController {
    private final EventService eventService;
    @GetMapping("/")
    public String welcome(Model model) {
        Event past_event = eventService.getPastTournament();
        Event current_event = eventService.getCurrentEvent();
        Event future_event = eventService.getNextEvent();

        model.addAttribute("past_event", past_event);
        model.addAttribute("current_event", current_event);
        model.addAttribute("future_event", future_event);

        return "index";
    }

    @GetMapping("/template")
    public String designTemplate() {
        return "design-template";
    }
}
