package ru.sfedu.fantazy.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.sfedu.fantazy.dto.AthleteAndPoints;
import ru.sfedu.fantazy.dto.OverviewTeamDto;
import ru.sfedu.fantazy.dto.TeamDto;
import ru.sfedu.fantazy.model.Athlete;
import ru.sfedu.fantazy.model.Event;
import ru.sfedu.fantazy.model.Team;
import ru.sfedu.fantazy.service.AthleteService;
import ru.sfedu.fantazy.service.ResultsService;
import ru.sfedu.fantazy.service.TeamService;
import ru.sfedu.fantazy.service.EventService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {
    private final EventService eventService;
    private final AthleteService athleteService;
    private final TeamService teamService;
    private final ResultsService resultsService;
    @GetMapping("/create-team/{id}")
    public String createTeam(@PathVariable("id") long eventId, Model model) {
        Event event = eventService.getById(eventId);
        List<Athlete> athletes = athleteService.getAll();
        List<Long> selected_athletes = new ArrayList<>();
        model.addAttribute("event", event);
        model.addAttribute("availableAthletes", athletes);
        model.addAttribute("selectedAthletes", selected_athletes);
        return "create-team";
    }

    @PostMapping("/create-team")
    public String createTeam(@RequestParam("name") String teamName,
                             @RequestParam("eventId") long eventId,
                             @RequestParam("selectedAthleteIds") List<Long> athleteIds,
                             Model model) {
        TeamDto teamDto = TeamDto.builder()
                .name(teamName)
                .eventId(eventId)
                .athleteIds(athleteIds)
                .build();
        Team team = teamService.addNewTeam(teamDto);
        Event event = eventService.getById(eventId);
        model.addAttribute("event", event);
        model.addAttribute("team", team);
        return "redirect:/user/team/" + eventId;
    }

    @GetMapping("/team/{id}")
    public String getTeam(@PathVariable("id") long eventId, Model model) {
        Event event = eventService.getById(eventId);
        Team team = teamService.getTeamByEventAndUser(event);
        OverviewTeamDto overviewTeamDto = teamService.getTeamAndPointsByTeam(team);
        model.addAttribute("overviewTeam", overviewTeamDto);
        model.addAttribute("event", event);
        return "user-team";
    }
}
