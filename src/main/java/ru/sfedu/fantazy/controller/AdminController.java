package ru.sfedu.fantazy.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.sfedu.fantazy.model.Athlete;
import ru.sfedu.fantazy.model.Event;
import ru.sfedu.fantazy.model.Race;
import ru.sfedu.fantazy.model.User;
import ru.sfedu.fantazy.repository.AthleteRepository;
import ru.sfedu.fantazy.service.*;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@Controller
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/admin")
public class AdminController {
    private final AthleteRepository athleteRepository;
    private final UploadService uploadService;
    private final RaceService raceService;
    private final AthleteService athleteService;
    private final EventService eventService;
    private final UserService userService;

    @GetMapping("")
    public String adminPanel() {
        return "admin/panel";
    }

    @GetMapping("/upload")
    public String uploadRaceForm() {
        return "admin/upload-form";
    }

    @PostMapping("/upload")
    public String uploadFile(@RequestParam("file") MultipartFile file, Model model) {
        if (file.isEmpty()) {
            model.addAttribute("message_for_upload", "Please select a file to upload");
            return "admin/upload-form";
        }

        try {
            if(uploadService.uploadRace(file.getBytes()))
                model.addAttribute("message_for_upload", "Successfully uploaded '" + file.getOriginalFilename() + "'");
            else {
                model.addAttribute("message_for_upload","File upload '" + file.getOriginalFilename() + "' FAILED");
            }
        } catch (IOException e) {
            model.addAttribute("message_for_upload","bad file");
            log.error("FileUpload Error");
            log.error(e.getClass().getName() + ": " + e.getMessage());
        }

        return "admin/upload-form";
    }

    @GetMapping("/add-event")
    public String addEventForm() {
        return "admin/add-event-form";
    }

    @PostMapping("/add-event")
    public String addEvent(@RequestParam("city_name") String cityName,
                           @RequestParam("start_date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                           @RequestParam("end_date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate,
                           Model model) {
        Event event = Event.builder()
                .city(cityName)
                .startDate(startDate)
                .endDate(endDate)
                .build();
        if(uploadService.addEvent(event)) {
            model.addAttribute("message_for_create_event", "Event added");
        }
        else {
            model.addAttribute("message_for_create_event", "Event adding failed");
        }
        return "admin/add-event-form";
    }

    @GetMapping("/users")
    public String getUsers(Model model) {
        List<User> users = userService.getAll();
        model.addAttribute("users", users);
        return "admin/user-list";
    }

    @GetMapping("/event-races/{id}")
    public String getRacesByEvent(@PathVariable("id") long id, Model model){
        List<Race> races = raceService.getAllByEventId(id);
        model.addAttribute("races", races);

        return "admin/race-list-admin";
    }

    @GetMapping("/race-create")
    public String createRaceForm(Race race){
        return "race-create";
    }

    @PostMapping("/race-create")
    public String createRace(Race race){
        raceService.saveRace(race);
        return "redirect:/races";
    }

    @GetMapping("/race-delete/{id}")
    public String deleteRace(@PathVariable("id") long raceId){
        Race race = raceService.getById(raceId);
        raceService.deleteById(race.getId());
        return "redirect:/admin/races/" + race.getEvent().getId();
    }

    @GetMapping("/race-update/{id}")
    public String updateRaceForm(@PathVariable("id") Long id, Model model){
        Race race = raceService.getById(id);
        model.addAttribute("race", race);
        return "admin/race-update";
    }

    @PostMapping("/race-update")
    public String updateRace(Race race){
        raceService.saveRace(race);
        return "redirect:/races";
    }


    @GetMapping("/events")
    public String findAllEvents(Model model){
        List<Event> events = eventService.getAll();
        model.addAttribute("events", events);

        return "admin/event-list-admin";
    }

    @GetMapping("/event-delete/{id}")
    public String deleteEvent(@PathVariable("id") long id){
        eventService.deleteById(id);
        return "redirect:/admin/events";
    }

    @GetMapping("/event-update/{id}")
    public String updateEventForm(@PathVariable("id") Long id, Model model){
        Event event = eventService.getById(id);
        model.addAttribute("event", event);
        return "admin/event-update";
    }

    @PostMapping("/event-update")
    public String updateEvent(Event event){
        eventService.saveEvent(event);
        return "redirect:/admin/events";
    }

    @GetMapping("/athletes")
    public String manageAthletes(Model model) {
        List<Athlete> athletes = athleteService.getAll();
        model.addAttribute("athletes", athletes);
        return "admin/athlete-list-admin";
    }

    @GetMapping("/add-photo-athlete/{id}")
    public String addPhotoForm(@PathVariable("id") Long id, Model model){
        model.addAttribute("athleteId", id);
        return "admin/add-photo-form";
    }

    @PostMapping("/add-photo-athlete")
    public String addPhoto(@RequestParam("photo") MultipartFile photo,
                           @RequestParam("athleteId") long athleteId) {
        boolean success = athleteService.addPhoto(athleteId, photo);
        if (success) {
            return "redirect:/admin/athletes";
        } else {
            return "redirect:/admin/add-photo-athlete/" + athleteId;
        }
    }
}
