package ru.sfedu.fantazy.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.sfedu.fantazy.dto.OverviewTeamDto;
import ru.sfedu.fantazy.dto.TeamAndPointsDto;
import ru.sfedu.fantazy.model.*;
import ru.sfedu.fantazy.service.*;

import java.util.List;

@Controller
@RequiredArgsConstructor
@Slf4j
public class GuestController {
    private final ResultsService resultsService;
    private final TeamService teamService;
    private final RaceService raceService;
    private final AthleteService athleteService;
    private final EventService eventService;

    @GetMapping("/results/{id}")
    public String checkResults(@PathVariable("id") long raceId, Model model) {
        List<Result> list = resultsService.getAllResultsByRace(raceId);
        Race race = raceService.getById(raceId);
        model.addAttribute("results", list);
        model.addAttribute("race", race);
        return "results-page";
    }

    @GetMapping("/leaderboard/{id}")
    public String getStandingsByEvent(@PathVariable("id") long eventId, Model model){
        List<TeamAndPointsDto> teamAndPointsDtoList = teamService.getTeamsAndPointsListByEventId(eventId);
        model.addAttribute("teamsAndPoints", teamAndPointsDtoList);
        return "leaderboard";
    }

    @GetMapping("/team-overview/{id}")
    public String overviewTeam(@PathVariable("id") long teamId, Model model) {
        Team team = teamService.getTeamById(teamId);
        OverviewTeamDto overviewTeamDto = teamService.getTeamAndPointsByTeam(team);
        model.addAttribute("overviewTeam", overviewTeamDto);
        return "overview-team";
    }
    @GetMapping("/races")
    public String findAllRaces(Model model) {
        List<Race> races = raceService.getAll();
        model.addAttribute("races", races);

        return "race-list";
    }

    @GetMapping("/event-races/{id}")
    public String getRacesByEventId(@PathVariable("id") long eventId, Model model) {
        List<Race> races = raceService.getAllByEventId(eventId);
        model.addAttribute("races", races);
        return "race-list";
    }

    @GetMapping("/athletes")
    public String findAllAthletes(Model model) {
        List<Athlete> athletes = athleteService.getAll();
        model.addAttribute("athletes", athletes);

        return "athlete-list";
    }

    @GetMapping("/events")
    public String allEvents(Model model) {
        List<Event> eventList = eventService.getAll();
        model.addAttribute("events", eventList);
        return "event-list";
    }
}
