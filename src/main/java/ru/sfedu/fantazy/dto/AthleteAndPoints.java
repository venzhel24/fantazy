package ru.sfedu.fantazy.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import ru.sfedu.fantazy.model.Athlete;

@Data
@Builder
@AllArgsConstructor
public class AthleteAndPoints {
    private Athlete athlete;
    private int points;
}
