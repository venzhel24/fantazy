package ru.sfedu.fantazy.dto;

import lombok.Builder;
import lombok.Data;
import ru.sfedu.fantazy.model.Team;

@Data
@Builder
public class TeamAndPointsDto {
    private Team team;
    private int totalPoints;
}
