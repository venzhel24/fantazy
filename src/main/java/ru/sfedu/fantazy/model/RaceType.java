package ru.sfedu.fantazy.model;

public enum RaceType {
    INDIVIDUAL,
    SPRINT,
    PURSUIT,
    MASS_START
}
