// Move athlete(s) from one select element to another
function moveAthlete(from, to) {
  if (to == document.getElementById('selectedAthletes') && to.length >= 3) {
    alert('You can only select three athletes.');
    return;
  }

  for (var i = from.options.length - 1; i >= 0; i--) {
    var option = from.options[i];
    if (option.selected) {
      to.add(option);
    }
  }
}

// Submit the team form after validating that the team name is not empty and exactly three athletes are selected
function submitForm() {
  var teamName = document.getElementById('teamName').value.trim();
  var selectedAthletes = document.getElementById('selectedAthletes').options;

  // Check that the team name is not empty
  if (teamName === '') {
    alert('Please enter a team name.');
    return;
  }

  // Check that exactly three athletes are selected
  if (selectedAthletes.length !== 3) {
    alert('Please select exactly three athletes.');
    return;
  }

  // Create hidden input fields for each selected athlete ID and append them to the form
  for (var i = 0; i < selectedAthletes.length; i++) {
    var input = document.createElement('input');
    input.type = 'hidden';
    input.name = 'selectedAthleteIds';
    input.value = selectedAthletes[i].value;
    document.getElementById('teamForm').appendChild(input);
  }

  // Submit the form
  document.getElementById('teamForm').submit();
}