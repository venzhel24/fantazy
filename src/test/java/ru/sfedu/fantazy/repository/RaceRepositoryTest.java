package ru.sfedu.fantazy.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.sfedu.fantazy.model.Event;
import ru.sfedu.fantazy.model.Race;
import ru.sfedu.fantazy.model.RaceType;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RaceRepositoryTest {
    @Mock
    private RaceRepository raceRepository;

    @Test
    public void testFindByEventId_RacesFound() {
        // Mock data
        long eventId = 12345L;
        Race race1 = new Race();
        // Set up any required properties of race1
        Race race2 = new Race();
        // Set up any required properties of race2

        List<Race> expectedRaces = Arrays.asList(race1, race2);

        // Define the behavior of the mocked repository
        when(raceRepository.findByEventId(eventId)).thenReturn(expectedRaces);

        // Invoke the method being tested
        List<Race> result = raceRepository.findByEventId(eventId);

        // Verify the result
        assertEquals(expectedRaces, result);

        // Optionally, verify the method invocation
        verify(raceRepository, times(1)).findByEventId(eventId);
    }

    @Test
    public void testFindByEventId_NoRacesFound() {
        // Mock data
        long eventId = 12345L;

        // Define the behavior of the mocked repository
        when(raceRepository.findByEventId(eventId)).thenReturn(Collections.emptyList());

        // Invoke the method being tested
        List<Race> result = raceRepository.findByEventId(eventId);

        // Verify the result
        assertTrue(result.isEmpty());

        // Optionally, verify the method invocation
        verify(raceRepository, times(1)).findByEventId(eventId);
    }

    @Test
    public void testExistsByEventAndRaceType_Exists() {
        // Mock data
        Event event = new Event();
        RaceType raceType = RaceType.PURSUIT;

        // Define the behavior of the mocked repository
        when(raceRepository.existsByEventAndRaceType(event, raceType)).thenReturn(true);

        // Invoke the method being tested
        boolean result = raceRepository.existsByEventAndRaceType(event, raceType);

        // Verify the result
        assertTrue(result);

        // Optionally, verify the method invocation
        verify(raceRepository, times(1)).existsByEventAndRaceType(event, raceType);
    }

    @Test
    public void testExistsByEventAndRaceType_DoesNotExist() {
        // Mock data
        Event event = new Event();
        RaceType raceType = RaceType.PURSUIT;

        // Define the behavior of the mocked repository
        when(raceRepository.existsByEventAndRaceType(event, raceType)).thenReturn(false);

        // Invoke the method being tested
        boolean result = raceRepository.existsByEventAndRaceType(event, raceType);

        // Verify the result
        assertFalse(result);

        // Optionally, verify the method invocation
        verify(raceRepository, times(1)).existsByEventAndRaceType(event, raceType);
    }


}