package ru.sfedu.fantazy.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.sfedu.fantazy.model.Event;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class EventRepositoryTest {

    @Mock
    private EventRepository eventRepository;

    @Test
    public void testFindCurrentEvent() {
        // Arrange
        LocalDate today = LocalDate.now();
        Event event = new Event();
        event.setId(1L);
        event.setCity("Test Event");
        event.setStartDate(today.minusDays(1));
        event.setEndDate(today.plusDays(1));
        when(eventRepository.findCurrentEvent(today)).thenReturn(Optional.of(event));

        // Act
        Optional<Event> result = eventRepository.findCurrentEvent(today);

        // Assert
        assertTrue(result.isPresent());
        assertEquals(1L, result.get().getId());
        assertEquals("Test Event", result.get().getCity());
        assertEquals(today.minusDays(1), result.get().getStartDate());
        assertEquals(today.plusDays(1), result.get().getEndDate());
        verify(eventRepository, times(1)).findCurrentEvent(today);
    }

    @Test
    public void testFindCurrentEventNotFound() {
        // Arrange
        LocalDate today = LocalDate.now();
        when(eventRepository.findCurrentEvent(today)).thenReturn(Optional.empty());

        // Act
        Optional<Event> result = eventRepository.findCurrentEvent(today);

        // Assert
        assertFalse(result.isPresent());
        verify(eventRepository, times(1)).findCurrentEvent(today);
    }


    @Test
    public void testFindNextEvents() {
        // Arrange
        LocalDate today = LocalDate.now();
        List<Event> expectedEvents = new ArrayList<>();
        Event event1 = new Event();
        event1.setId(1L);
        event1.setCity("Test Event 1");
        event1.setStartDate(today.plusDays(1));
        expectedEvents.add(event1);
        Event event2 = new Event();
        event2.setId(2L);
        event2.setCity("Test Event 2");
        event2.setStartDate(today.plusDays(2));
        expectedEvents.add(event2);
        when(eventRepository.findNextEvents(today)).thenReturn(expectedEvents);

        // Act
        List<Event> result = eventRepository.findNextEvents(today);

        // Assert
        assertEquals(2, result.size());
        assertEquals(1L, result.get(0).getId());
        assertEquals("Test Event 1", result.get(0).getCity());
        assertEquals(today.plusDays(1), result.get(0).getStartDate());
        assertEquals(2L, result.get(1).getId());
        assertEquals("Test Event 2", result.get(1).getCity());
        assertEquals(today.plusDays(2), result.get(1).getStartDate());
        verify(eventRepository, times(1)).findNextEvents(today);
    }

    @Test
    public void testFindNextEventsNoResults() {
        // Arrange
        LocalDate today = LocalDate.now();
        List<Event> expectedEvents = new ArrayList<>();
        when(eventRepository.findNextEvents(today)).thenReturn(expectedEvents);

        // Act
        List<Event> result = eventRepository.findNextEvents(today);

        // Assert
        assertTrue(result.isEmpty());
        verify(eventRepository, times(1)).findNextEvents(today);
    }


    @Test
    public void testFindLastEvents() {
        // Arrange
        LocalDate today = LocalDate.now();
        List<Event> expectedEvents = new ArrayList<>();
        Event event1 = new Event();
        event1.setId(1L);
        event1.setCity("Test Event 1");
        event1.setEndDate(today.minusDays(1));
        expectedEvents.add(event1);
        Event event2 = new Event();
        event2.setId(2L);
        event2.setCity("Test Event 2");
        event2.setEndDate(today.minusDays(2));
        expectedEvents.add(event2);
        when(eventRepository.findLastEvents(today)).thenReturn(expectedEvents);

        // Act
        List<Event> result = eventRepository.findLastEvents(today);

        // Assert
        assertEquals(2, result.size());
        assertEquals(1L, result.get(0).getId());
        assertEquals("Test Event 1", result.get(0).getCity());
        assertEquals(today.minusDays(1), result.get(0).getEndDate());
        assertEquals(2L, result.get(1).getId());
        assertEquals("Test Event 2", result.get(1).getCity());
        assertEquals(today.minusDays(2), result.get(1).getEndDate());
        verify(eventRepository, times(1)).findLastEvents(today);
    }

    @Test
    public void testFindLastEventsNoResults() {
        // Arrange
        LocalDate today = LocalDate.now();
        List<Event> expectedEvents = new ArrayList<>();
        when(eventRepository.findLastEvents(today)).thenReturn(expectedEvents);

        // Act
        List<Event> result = eventRepository.findLastEvents(today);

        // Assert
        assertTrue(result.isEmpty());
        verify(eventRepository, times(1)).findLastEvents(today);
    }


    @Test
    public void testFindByCityAndStartDateYear_EventFound() {
        // Mock data
        String city = "OBERHOF";
        int year = 2023;
        Event event = new Event();

        // Define the behavior of the mocked repository
        when(eventRepository.findByCityAndYear(city, year)).thenReturn(Optional.of(event));

        // Invoke the method being tested
        Optional<Event> result = eventRepository.findByCityAndYear(city, year);

        // Verify the result
        assertTrue(result.isPresent());
        assertEquals(event, result.get());

        // Optionally, verify the method invocation
        verify(eventRepository, times(1)).findByCityAndYear(city, year);
    }

    @Test
    public void testFindByCityAndStartDateYear_EventNotFound() {
        // Mock data
        String city = "OBERHOF";
        int year = 2023;

        // Define the behavior of the mocked repository
        when(eventRepository.findByCityAndYear(city, year)).thenReturn(Optional.empty());

        // Invoke the method being tested
        Optional<Event> result = eventRepository.findByCityAndYear(city, year);

        // Verify the result
        assertFalse(result.isPresent());

        // Optionally, verify the method invocation
        verify(eventRepository, times(1)).findByCityAndYear(city, year);
    }
}