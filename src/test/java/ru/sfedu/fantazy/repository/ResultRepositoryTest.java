package ru.sfedu.fantazy.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.sfedu.fantazy.model.Race;
import ru.sfedu.fantazy.model.Result;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ResultRepositoryTest {

    @Mock
    private ResultRepository resultRepository;

    @Test
    public void testFindPointsByAthleteIdAndEventId() {
        // Arrange
        Long athleteId = 1L;
        Long eventId = 2L;
        Integer expectedPoints = 10;
        when(resultRepository.findPointsByAthleteIdAndEventId(athleteId, eventId)).thenReturn(Optional.of(expectedPoints));

        // Act
        Optional<Integer> result = resultRepository.findPointsByAthleteIdAndEventId(athleteId, eventId);

        // Assert
        assertTrue(result.isPresent());
        assertEquals(expectedPoints, result.get());
        verify(resultRepository, times(1)).findPointsByAthleteIdAndEventId(athleteId, eventId);
    }

    @Test
    public void testFindPointsByAthleteIdAndEventIdNoResult() {
        // Arrange
        Long athleteId = 1L;
        Long eventId = 2L;
        when(resultRepository.findPointsByAthleteIdAndEventId(athleteId, eventId)).thenReturn(Optional.empty());

        // Act
        Optional<Integer> result = resultRepository.findPointsByAthleteIdAndEventId(athleteId, eventId);

        // Assert
        assertFalse(result.isPresent());
        verify(resultRepository, times(1)).findPointsByAthleteIdAndEventId(athleteId, eventId);
    }

    @Test
    public void testFindResultsByRace_ResultsFound() {
        // Mock data
        Race race = new Race();
        // Set up any required properties of the race
        Result result1 = new Result();
        // Set up any required properties of result1
        Result result2 = new Result();
        // Set up any required properties of result2

        List<Result> expectedResults = Arrays.asList(result1, result2);

        // Define the behavior of the mocked repository
        when(resultRepository.findResultsByRace(race)).thenReturn(expectedResults);

        // Invoke the method being tested
        List<Result> results = resultRepository.findResultsByRace(race);

        // Verify the result
        assertEquals(expectedResults, results);

        // Optionally, verify the method invocation
        verify(resultRepository, times(1)).findResultsByRace(race);
    }

    @Test
    public void testFindResultsByRace_NoResultsFound() {
        // Mock data
        Race race = new Race();
        // Set up any required properties of the race

        // Define the behavior of the mocked repository
        when(resultRepository.findResultsByRace(race)).thenReturn(Collections.emptyList());

        // Invoke the method being tested
        List<Result> results = resultRepository.findResultsByRace(race);

        // Verify the result
        assertTrue(results.isEmpty());

        // Optionally, verify the method invocation
        verify(resultRepository, times(1)).findResultsByRace(race);
    }

}