package ru.sfedu.fantazy.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.sfedu.fantazy.model.User;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserRepositoryTest {
    @Mock
    private UserRepository userRepository;

    @Test
    public void testFindByUsername_UserFound() {
        // Mock data
        String username = "john_doe";
        User user = new User();

        // Define the behavior of the mocked repository
        when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));

        // Invoke the method being tested
        Optional<User> result = userRepository.findByUsername(username);

        // Verify the result
        assertTrue(result.isPresent());
        assertEquals(user, result.get());

        // Optionally, verify the method invocation
        verify(userRepository, times(1)).findByUsername(username);
    }

    @Test
    public void testFindByUsername_UserNotFound() {
        // Mock data
        String username = "nonexistent_user";

        // Define the behavior of the mocked repository
        when(userRepository.findByUsername(username)).thenReturn(Optional.empty());

        // Invoke the method being tested
        Optional<User> result = userRepository.findByUsername(username);

        // Verify the result
        assertFalse(result.isPresent());

        // Optionally, verify the method invocation
        verify(userRepository, times(1)).findByUsername(username);
    }

    @Test
    public void testFindByEmail_UserFound() {
        // Mock data
        String email = "john.doe@example.com";
        User user = new User();

        // Define the behavior of the mocked repository
        when(userRepository.findByEmail(email)).thenReturn(Optional.of(user));

        // Invoke the method being tested
        Optional<User> result = userRepository.findByEmail(email);

        // Verify the result
        assertTrue(result.isPresent());
        assertEquals(user, result.get());

        // Optionally, verify the method invocation
        verify(userRepository, times(1)).findByEmail(email);
    }

    @Test
    public void testFindByEmail_UserNotFound() {
        // Mock data
        String email = "nonexistent@example.com";

        // Define the behavior of the mocked repository
        when(userRepository.findByEmail(email)).thenReturn(Optional.empty());

        // Invoke the method being tested
        Optional<User> result = userRepository.findByEmail(email);

        // Verify the result
        assertFalse(result.isPresent());

        // Optionally, verify the method invocation
        verify(userRepository, times(1)).findByEmail(email);
    }


}