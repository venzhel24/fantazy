package ru.sfedu.fantazy.repository;

import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;
import ru.sfedu.fantazy.model.Athlete;
import ru.sfedu.fantazy.model.Event;
import ru.sfedu.fantazy.model.Team;
import ru.sfedu.fantazy.model.User;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TeamRepositoryTest {
    @Mock
    private TeamRepository teamRepository;

    @Test
    public void testFindByEventIdAndUserId_TeamFound() {
        // Mock data
        long eventId = 12345L;
        long userId = 67890L;
        Team team = new Team();

        // Define the behavior of the mocked repository
        when(teamRepository.findByEventIdAndUserId(eventId, userId)).thenReturn(Optional.of(team));

        // Invoke the method being tested
        Optional<Team> result = teamRepository.findByEventIdAndUserId(eventId, userId);

        // Verify the result
        assertTrue(result.isPresent());
        assertEquals(team, result.get());

        // Optionally, verify the method invocation
        verify(teamRepository, times(1)).findByEventIdAndUserId(eventId, userId);
    }

    @Test
    public void testFindByEventIdAndUserId_TeamNotFound() {
        // Mock data
        long eventId = 12345L;
        long userId = 67890L;

        // Define the behavior of the mocked repository
        when(teamRepository.findByEventIdAndUserId(eventId, userId)).thenReturn(Optional.empty());

        // Invoke the method being tested
        Optional<Team> result = teamRepository.findByEventIdAndUserId(eventId, userId);

        // Verify the result
        assertFalse(result.isPresent());

        // Optionally, verify the method invocation
        verify(teamRepository, times(1)).findByEventIdAndUserId(eventId, userId);
    }

    @Test
    public void testFindByEventId_TeamsFound() {
        // Mock data
        long eventId = 12345L;
        Team team1 = new Team();
        Team team2 = new Team();

        List<Team> expectedTeams = Arrays.asList(team1, team2);

        // Define the behavior of the mocked repository
        when(teamRepository.findByEventId(eventId)).thenReturn(expectedTeams);

        // Invoke the method being tested
        List<Team> result = teamRepository.findByEventId(eventId);

        // Verify the result
        assertEquals(expectedTeams, result);

        // Optionally, verify the method invocation
        verify(teamRepository, times(1)).findByEventId(eventId);
    }

    @Test
    public void testFindByEventId_NoTeamsFound() {
        // Mock data
        long eventId = 12345L;

        // Define the behavior of the mocked repository
        when(teamRepository.findByEventId(eventId)).thenReturn(Collections.emptyList());

        // Invoke the method being tested
        List<Team> result = teamRepository.findByEventId(eventId);

        // Verify the result
        assertTrue(result.isEmpty());

        // Optionally, verify the method invocation
        verify(teamRepository, times(1)).findByEventId(eventId);
    }

    @Test
    public void testExistsTeamByEventIdAndUserId_TeamExists() {
        // Mock data
        long eventId = 12345L;
        long userId = 67890L;

        // Define the behavior of the mocked repository
        when(teamRepository.existsTeamByEventIdAndUserId(eventId, userId)).thenReturn(true);

        // Invoke the method being tested
        boolean result = teamRepository.existsTeamByEventIdAndUserId(eventId, userId);

        // Verify the result
        assertTrue(result);

        // Optionally, verify the method invocation
        verify(teamRepository, times(1)).existsTeamByEventIdAndUserId(eventId, userId);
    }

    @Test
    public void testExistsTeamByEventIdAndUserId_TeamDoesNotExist() {
        // Mock data
        long eventId = 12345L;
        long userId = 67890L;

        // Define the behavior of the mocked repository
        when(teamRepository.existsTeamByEventIdAndUserId(eventId, userId)).thenReturn(false);

        // Invoke the method being tested
        boolean result = teamRepository.existsTeamByEventIdAndUserId(eventId, userId);

        // Verify the result
        assertFalse(result);

        // Optionally, verify the method invocation
        verify(teamRepository, times(1)).existsTeamByEventIdAndUserId(eventId, userId);
    }


}