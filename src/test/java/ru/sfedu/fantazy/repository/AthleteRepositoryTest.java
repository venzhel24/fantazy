package ru.sfedu.fantazy.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.sfedu.fantazy.model.Athlete;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AthleteRepositoryTest {

    @Mock
    private AthleteRepository athleteRepository;

    @Test
    public void testFindAthleteByName_AthleteFound() {
        // Mock data
        String name = "John Doe";
        Athlete athlete = new Athlete();
        // Set up any required properties of the athlete

        // Define the behavior of the mocked repository
        when(athleteRepository.findAthleteByName(name)).thenReturn(Optional.of(athlete));

        // Invoke the method being tested
        Optional<Athlete> result = athleteRepository.findAthleteByName(name);

        // Verify the result
        assertTrue(result.isPresent());
        assertEquals(athlete, result.get());

        // Optionally, verify the method invocation
        verify(athleteRepository, times(1)).findAthleteByName(name);
    }

    @Test
    public void testFindAthleteByName_AthleteNotFound() {
        // Mock data
        String name = "John Doe";

        // Define the behavior of the mocked repository
        when(athleteRepository.findAthleteByName(name)).thenReturn(Optional.empty());

        // Invoke the method being tested
        Optional<Athlete> result = athleteRepository.findAthleteByName(name);

        // Verify the result
        assertFalse(result.isPresent());

        // Optionally, verify the method invocation
        verify(athleteRepository, times(1)).findAthleteByName(name);
    }

}