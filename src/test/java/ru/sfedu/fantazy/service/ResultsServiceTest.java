package ru.sfedu.fantazy.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.sfedu.fantazy.dto.AthleteAndPoints;
import ru.sfedu.fantazy.model.Athlete;
import ru.sfedu.fantazy.repository.AthleteRepository;
import ru.sfedu.fantazy.repository.ResultRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ResultsServiceTest {

    @Mock
    private AthleteRepository athleteRepository;

    @Mock
    private ResultRepository resultRepository;

    @InjectMocks
    private ResultsService resultsService;

    @Test
    void returnPointsByEventAndAthleteIds() {
        // given
        long eventId = 1L;
        List<Long> athleteIds = Arrays.asList(2L, 3L);

        Athlete athlete1 = new Athlete();
        athlete1.setId(2L);
        athlete1.setName("John");
        athlete1.setCountry("USA");

        Athlete athlete2 = new Athlete();
        athlete1.setId(3L);
        athlete1.setName("Jane");
        athlete1.setCountry("RUS");

        List<AthleteAndPoints> expectedAthleteAndPointsList = Arrays.asList(
                new AthleteAndPoints(athlete1, 10),
                new AthleteAndPoints(athlete2, 20));
        when(athleteRepository.findById(2L)).thenReturn(Optional.of(athlete1));
        when(athleteRepository.findById(3L)).thenReturn(Optional.of(athlete2));
        when(resultRepository.findPointsByAthleteIdAndEventId(2L, 1L)).thenReturn(Optional.of(10));
        when(resultRepository.findPointsByAthleteIdAndEventId(3L, 1L)).thenReturn(Optional.of(20));

        // when
        List<AthleteAndPoints> athleteAndPointsList = resultsService.getPointsByEventAndAthleteIds(eventId, athleteIds);

        // then
        assertNotNull(athleteAndPointsList);
        assertEquals(expectedAthleteAndPointsList, athleteAndPointsList);
    }

    @Test
    void findPointsByAthleteIdAndEventIdReturnNullWhenAthleteNotFound() {
        // given
        long eventId = 1L;
        List<Long> athleteIds = Arrays.asList(2L, 3L);
        when(athleteRepository.findById(2L)).thenReturn(Optional.empty());

        // when
        List<AthleteAndPoints> athleteAndPointsList = resultsService.getPointsByEventAndAthleteIds(eventId, athleteIds);

        // then
        assertNull(athleteAndPointsList);
    }

    @Test
    void findPointsByAthleteIdAndEventIdReturnNullWhenNoPoints() {
        // given
        long eventId = 1L;
        List<Long> athleteIds = Arrays.asList(2L, 3L);

        Athlete athlete1 = new Athlete();
        athlete1.setId(2L);
        athlete1.setName("John");
        athlete1.setCountry("USA");

        Athlete athlete2 = new Athlete();
        athlete1.setId(3L);
        athlete1.setName("Jane");
        athlete1.setCountry("RUS");

        List<AthleteAndPoints> expectedAthleteAndPointsList = Arrays.asList(
                new AthleteAndPoints(athlete1, -1),
                new AthleteAndPoints(athlete2, -1));
        when(athleteRepository.findById(2L)).thenReturn(Optional.of(athlete1));
        when(athleteRepository.findById(3L)).thenReturn(Optional.of(athlete2));
        when(resultRepository.findPointsByAthleteIdAndEventId(2L, 1L)).thenReturn(Optional.empty());
        when(resultRepository.findPointsByAthleteIdAndEventId(3L, 1L)).thenReturn(Optional.empty());

        // when
        List<AthleteAndPoints> athleteAndPointsList = resultsService.getPointsByEventAndAthleteIds(eventId, athleteIds);

        // then
        assertNotNull(athleteAndPointsList);
    }
}