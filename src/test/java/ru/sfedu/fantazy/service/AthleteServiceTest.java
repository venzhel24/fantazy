package ru.sfedu.fantazy.service;

import org.apache.tomcat.util.codec.binary.Base64;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;
import ru.sfedu.fantazy.model.Athlete;
import ru.sfedu.fantazy.repository.AthleteRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AthleteServiceTest {

    @Mock
    private AthleteRepository athleteRepository;

    @InjectMocks
    private AthleteService athleteService;

    @BeforeEach
    public void setup() {
        athleteService = new AthleteService(athleteRepository);
    }

    @Test
    public void testGetAll_WithAthletes_ReturnsList() {
        // Arrange
        List<Athlete> athletes = new ArrayList<>();
        Athlete athlete1 = new Athlete();
        athlete1.setName("John Doe");
        athletes.add(athlete1);

        Athlete athlete2 = new Athlete();
        athlete2.setName("Jane Smith");
        athletes.add(athlete2);

        when(athleteRepository.findAll()).thenReturn(athletes);

        // Act
        List<Athlete> result = athleteService.getAll();

        // Assert
        assertNotNull(result);
        assertEquals(2, result.size());
        assertEquals("John Doe", result.get(0).getName());
        assertEquals("Jane Smith", result.get(1).getName());
    }

    @Test
    public void testGetAll_WithNoAthletes_ReturnsNull() {
        // Arrange
        List<Athlete> emptyList = new ArrayList<>();
        when(athleteRepository.findAll()).thenReturn(emptyList);

        // Act
        List<Athlete> result = athleteService.getAll();

        // Assert
        assertNull(result);
    }

    @Test
    public void testAddPhoto_WithValidAthleteIdAndNonEmptyPhoto_ReturnsTrue() throws Exception {
        // Arrange
        long athleteId = 1L;
        byte[] photoData = "SamplePhotoData".getBytes();
        MockMultipartFile photo = new MockMultipartFile("photo.jpg", photoData);

        Athlete athlete = new Athlete();
        athlete.setId(athleteId);

        when(athleteRepository.findById(athleteId)).thenReturn(Optional.of(athlete));

        // Act
        boolean result = athleteService.addPhoto(athleteId, photo);

        // Assert
        assertTrue(result);
        assertEquals(Base64.encodeBase64String(photoData), athlete.getPhoto());
        verify(athleteRepository, times(1)).save(athlete);
    }

    @Test
    public void testAddPhoto_WithInvalidAthleteId_ReturnsFalse() throws Exception {
        // Arrange
        long athleteId = 1L;
        MockMultipartFile photo = new MockMultipartFile("photo.jpg", "SamplePhotoData".getBytes());

        when(athleteRepository.findById(athleteId)).thenReturn(Optional.empty());

        // Act
        boolean result = athleteService.addPhoto(athleteId, photo);

        // Assert
        assertFalse(result);
        verify(athleteRepository, never()).save(any(Athlete.class));
    }

    @Test
    public void testAddPhoto_WithEmptyPhoto_ReturnsFalse() throws Exception {
        // Arrange
        long athleteId = 1L;
        MockMultipartFile photo = new MockMultipartFile("photo.jpg", new byte[0]);

        // Act
        boolean result = athleteService.addPhoto(athleteId, photo);

        // Assert
        assertFalse(result);
        verify(athleteRepository, never()).save(any(Athlete.class));
    }
}