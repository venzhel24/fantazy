| Name                                  |       Type       |
|---------------------------------------|:----------------:|
| [use-case for guest](#guest)          |     use-case     |
| [use-case for user](#user)            |     use-case     |
| [use-case for admin](#admin)          |     use-case     |
| [Class diagram](#class-diagram)       |  class-diagram   |
| [Activity diagram](#activity-diagram) | activity-diagram |

# Diagrams
## use-case
### Guest
![UCD.png](documentation/UCD_guest.png)

| use-case         |                                                                         detailing                                                                         |
|------------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------:|
| Get team         |                         Получить команду пользователя<br>**Входные параметры:** id команды<br>**Выходные параметры:** Объект Team                         |
| Get Leaderboard  |              Получить рейтинг пользователей по турниру<br>**Входные параметры:** id турнира<br>**Выходные параметры:** Список объектов Team               |
| Get event races  |                       Получить гонки турнира<br>**Входные параметры:** id Турнира<br>**Выходные параметры:** Список объектов Races                        |
| Get race results |                      Получить результаты гонки<br>**Входные параметры:** id гонки<br>**Выходные параметры:** Список объектов Result                       |

### User
![UCD.png](documentation/UCD_user.png)

| use-case         |                                                              detailing                                                               |
|------------------|:------------------------------------------------------------------------------------------------------------------------------------:|
| Create team      | Создать команду<br>**Входные параметры:** Список id спортсменов, пользователь, id турнира<br>**Выходные параметры:** Статус операции |
| Get my team      |                   Получить мою команду<br>**Входные параметры:** id турнира<br>**Выходные параметры:** Объект Team                   |

### Admin
![UCD.png](documentation/UCD_admin.png)

| use-case            |                                                            detailing                                                             |
|---------------------|:--------------------------------------------------------------------------------------------------------------------------------:|
| Upload race results |             Загрузить результаты гонки<br>**Входные параметры:** pdf файл<br>**Выходные параметры:** Статус операции             |
| Add race            |               Добавить гонку<br>**Входные параметры:** Тип гонки, дата<br>**Выходные параметры:** Статус операции                |
| Create tournament   | Создать турнир<br>**Входные параметры:** Название города, дата начала, дата окончания<br>**Выходные параметры:** Статус операции |
| Manage athlete      |     Управление спортсменами<br>**Входные параметры:** id спортсмена, тип операции<br>**Выходные параметры:** Статус операции     |

[use-case diagram documentation](https://plantuml.com/en/use-case-diagram)
## Class diagram
![ClassDiagram.png](documentation/ClassDiagramm.png)

[class diagram documentation](https://plantuml.com/en/class-diagram)

## Activity diagram
![ActivityDiagram.png](documentation/ActivityDiagram.png)